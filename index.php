<?php
  ini_set('display_errors', 1);
  require_once('TwitterAPIExchange.php');
  /** Set access tokens here - see: https://dev.twitter.com/apps/ **/
  $settings = array(
      'oauth_access_token' => "101153018-5EBD58PkjWV9nVNnzyjV5YxjTjhgRsHzaNFjbrTE",
      'oauth_access_token_secret' => "1ta7JgILICZszzhppzx66KwmFd1pQy2JqBMyBk23yJqlo",
      'consumer_key' => "UzIiSpAoRtSM3niJuKwSjM02d",
      'consumer_secret' => "rJtWTQWTJa2hTyU5TQR3alUKtaSM65muzSciJI2v4PQJnSKtz4"
  );

  $servername = "localhost";
  $username = "adzcer_twitterap";
  $password = "dp5Pvd,=s*Gk";
  $db = "adzcer_twitterapi";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Twitter API Sampler</title>
    <!-- Loading Bootstrap -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/jumbotron-narrow.css"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
  <div class="container">
    <div class="header">
      <h3 class="text-muted">Twitter API Tweet Counter</h3>
    </div>

    <div class="jumbotron">
      <h1>Twitter user: adzcer</h1>
<?php
  // Create connection
  $conn = new mysqli($servername, $username, $password, $db);

  // Check connection
  if ($conn->connect_error) {
      die("Connection failed: " . $conn->connect_error);
  } 
?>      
      <p class="lead">Connected to db successful...</p>
      <p class="lead">Interfacing Twitter API...</p>
<?php
  $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
  $getfield = '?screen_name=adzcer&count=500';
  $requestMethod = 'GET';
  $twitter = new TwitterAPIExchange($settings);
  $json = $twitter->setGetfield($getfield)
               ->buildOauth($url, $requestMethod)
               ->performRequest();
  $response = json_decode($json);
?>     
      <p class="lead">Clearing Dump Table...</p>
<?php
  $sql = "TRUNCATE TABLE dumptable";
  if ($conn->query($sql) === TRUE) {
      echo "<p class=\"lead\">Dump table cleared.</p>";
  } else {
      echo "<p class=\"lead\">Error: " . $sql . "<br>" . $conn->error."</p>";
  }
?>      
      <p class="lead">Inserting records...</p>
<?php
foreach ($response as $row) {
    $date = date_create($row->created_at);
    // echo date_format($date, 'Y-m-d');
    // echo $row->created_at;
    // echo '<br/>';

    $sql = "INSERT INTO dumptable (user, datetimeval, valuecount) VALUES ('adzcer', '".date_format($date, 'Y-m-d')."', NULL)";

    if ($conn->query($sql) === TRUE) {
        // echo "New record created successfully";
    } else {
        // echo "Error: " . $sql . "<br>" . $conn->error;
    }
}
?>    
      <p class="lead">Collating records...</p>
<?php
$sql = "select `datetimeval`,count(`datetimeval`) as tweetcount from dumptable group by `datetimeval` order by count(`datetimeval`) desc limit 1";
$result = $conn->query($sql);
if ($result->num_rows > 0) {
    // output data of each row
    echo "<p class=\"lead\">User: adzcer has most twitter post on:</p>";
    while($row = $result->fetch_assoc()) {
        echo "<p class=\"lead\">Date: " . $row["datetimeval"]. " - Tweet Count: " . $row["tweetcount"]."</p>";
    }
} else {
    echo "Something's gone wrong?";
}

?>      
    </div>

    <footer class="footer">
      <p>&copy; Some Company 2015</p>
    </footer>

  </div> <!-- /container -->
  <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
  </body>
</html>